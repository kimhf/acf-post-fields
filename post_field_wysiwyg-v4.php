<?php

class acf_post_field_wysiwyg extends acf_field {
	
	/*
	*  __construct
	*
	*  Set name / label needed for actions / filters
	*
	*  @since	1.0.0
	*  @date	23/01/13
	*/
	
	function __construct()
	{
		// vars
		$this->name = 'post_wysiwyg';
		$this->label = __("Post Wysiwyg Editor",'acf');
		$this->category = __('Post');
		$this->defaults = array(
			'toolbar'		=>	'full',
			'media_upload' 	=>	'yes',
			'default_value'	=>	'',
			'post_field_type'	=>	'content',
		);
		
		// do not delete!
    	parent::__construct();
	}
	   	
   	/*
	*  create_field()
	*
	*  Create the HTML interface for your field
	*
	*  @param	$field - an array holding all the field's data
	*
	*  @type	action
	*  @since	1.0.0
	*  @date	23/01/13
	*/
	
	function create_field( $field ) {
		$field['name'] = $field['post_field_type'];
		
		$settings = array();
		
		if ( $field['media_upload'] == 'no' ) {
			$settings['media_buttons'] = false;
		}
		if ( $field['toolbar'] == 'basic' ) {
			$settings['teeny'] = true;
		}
		
		wp_editor( $field['value'], $field['name'], $settings ); 
	}
	
	
	/*
	*  create_options()
	*
	*  Create extra options for your field. This is rendered when editing a field.
	*  The value of $field['name'] can be used (like bellow) to save extra data to the $field
	*
	*  @type	action
	*  @since	1.0.0
	*  @date	23/01/13
	*
	*  @param	$field	- an array holding all the field's data
	*/
	
	function create_options( $field ) {
		// vars
		$key = $field['name'];
		
		?>
<tr class="field_option field_option_<?php echo $this->name; ?>">
	<td class="label">
		<label><?php _e("Save to",'acf'); ?></label>
		<p><?php _e("How to save the field.",'acf') ?></p>
	</td>
	<td>
		<?php 
		do_action('acf/create_field', array(
			'type'	=>	'select',
			'name'	=>	'fields['.$key.'][post_field_type]',
			'value'	=>	$field['post_field_type'],
			'choices' => array(
				'content'	=>	__("Content",'acf'),
				'excerpt'	=>	__("Excerpt",'acf'),
				'title'	=>	__("Title",'acf')
			)
		));
		?>
	</td>
</tr>
<tr class="field_option field_option_<?php echo $this->name; ?>">
	<td class="label">
		<label><?php _e("Default Value",'acf'); ?></label>
		<p><?php _e("Appears when creating a new post",'acf') ?></p>
	</td>
	<td>
		<?php 
		do_action('acf/create_field', array(
			'type'	=>	'textarea',
			'name'	=>	'fields['.$key.'][default_value]',
			'value'	=>	$field['default_value'],
		));
		?>
	</td>
</tr>
<tr class="field_option field_option_<?php echo $this->name; ?>">
	<td class="label">
		<label><?php _e("Toolbar",'acf'); ?></label>
	</td>
	<td>
		<?php
		
		$toolbars = apply_filters( 'acf/fields/wysiwyg/toolbars', array() );
		$choices = array();
		
		if( is_array($toolbars) )
		{
			foreach( $toolbars as $k => $v )
			{
				$label = $k;
				$name = sanitize_title( $label );
				$name = str_replace('-', '_', $name);
				
				$choices[ $name ] = $label;
			}
		}
		
		do_action('acf/create_field', array(
			'type'	=>	'radio',
			'name'	=>	'fields['.$key.'][toolbar]',
			'value'	=>	$field['toolbar'],
			'layout'	=>	'horizontal',
			'choices' => $choices
		));
		?>
	</td>
</tr>
<tr class="field_option field_option_<?php echo $this->name; ?>">
	<td class="label">
		<label><?php _e("Show Media Upload Buttons?",'acf'); ?></label>
	</td>
	<td>
		<?php 
		do_action('acf/create_field', array(
			'type'	=>	'radio',
			'name'	=>	'fields['.$key.'][media_upload]',
			'value'	=>	$field['media_upload'],
			'layout'	=>	'horizontal',
			'choices' => array(
				'yes'	=>	__("Yes",'acf'),
				'no'	=>	__("No",'acf'),
			)
		));
		?>
	</td>
</tr>
		<?php
	}
	
	/*
	*  load_value()
	*
	*  This filter is appied to the $value after it is loaded from the db
	*
	*  @type	filter
	*  @since	1.0.0
	*  @date	23/01/13
	*
	*  @param	$value - the value found in the database
	*  @param	$post_id - the $post_id from which the value was loaded from
	*  @param	$field - the field array holding all the field options
	*
	*  @return	$value - the value to be saved in te database
	*/

	function load_value($value, $post_id, $field) {
		$value = content_field_load_value($value, $post_id, $field);
		return $value;
	}
}

new acf_post_field_wysiwyg();

?>