<?php

class acf_post_field_wysiwyg extends acf_field {
	
	/*
	*  __construct
	*
	*  Set name / label needed for actions / filters
	*
	*  @since	1.0.0
	*  @date	23/01/13
	*/
	
	function __construct()
	{
		// vars
		$this->name = 'post_wysiwyg';
		$this->label = __("Post Wysiwyg Editor",'acf');
		$this->category = __('Post');
		$this->defaults = array(
			'toolbar'		=>	'full',
			'media_upload' 	=>	'yes',
			'default_value'	=>	'',
			'post_field_type'	=>	'content',
		);
		
		// do not delete!
    	parent::__construct();
	}
	   	
   	/*
	*  render_field()
	*
	*  Create the HTML interface for your field
	*
	*  @param	$field (array) the $field being rendered
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/
	
	function render_field( $field ) {
		$field['name'] = $field['post_field_type'];
		
		$settings = array();
		
		if ( $field['media_upload'] == 'no' ) {
			$settings['media_buttons'] = false;
		}
		if ( $field['toolbar'] == 'basic' ) {
			$settings['teeny'] = true;
		}
		
		wp_editor( $field['value'], $field['name'], $settings ); 
	}
	
	/*
	*  get_toolbars
	*
	*  This function will return an array of toolbars for the WYSIWYG field
	*
	*  @type	function
	*  @date	18/04/2014
	*  @since	5.0.0
	*
	*  @param	n/a
	*  @return	(array)
	*/
	
   	function get_toolbars() {
   		
   		// global
   		global $wp_version;
   		
   		
   		// vars
   		$toolbars = array();
   		$editor_id = 'acf_content';
   		
   		
   		if( version_compare($wp_version, '3.9', '>=' ) ) {
   		
   			// Full
	   		$toolbars['Full'] = array(
	   			
	   			1 => apply_filters('mce_buttons', array('bold', 'italic', 'strikethrough', 'bullist', 'numlist', 'blockquote', 'hr', 'alignleft', 'aligncenter', 'alignright', 'link', 'unlink', 'wp_more', 'spellchecker', 'fullscreen', 'wp_adv' ), $editor_id),
	   			
	   			2 => apply_filters('mce_buttons_2', array( 'formatselect', 'underline', 'alignjustify', 'forecolor', 'pastetext', 'removeformat', 'charmap', 'outdent', 'indent', 'undo', 'redo', 'wp_help' ), $editor_id),
	   			
	   			3 => apply_filters('mce_buttons_3', array(), $editor_id),
	   			
	   			4 => apply_filters('mce_buttons_4', array(), $editor_id),
	   			
	   		);
	   		
	   		
	   		// Basic
	   		$toolbars['Basic'] = array(
	   			
	   			1 => apply_filters('teeny_mce_buttons', array('bold', 'italic', 'underline', 'blockquote', 'strikethrough', 'bullist', 'numlist', 'alignleft', 'aligncenter', 'alignright', 'undo', 'redo', 'link', 'unlink', 'fullscreen'), $editor_id),
	   			
	   		);
	   		  		
   		} else {
	   		
	   		// Full
	   		$toolbars['Full'] = array(
	   			
	   			1 => apply_filters('mce_buttons', array('bold', 'italic', 'strikethrough', 'bullist', 'numlist', 'blockquote', 'justifyleft', 'justifycenter', 'justifyright', 'link', 'unlink', 'wp_more', 'spellchecker', 'fullscreen', 'wp_adv' ), $editor_id),
	   			
	   			2 => apply_filters('mce_buttons_2', array( 'formatselect', 'underline', 'justifyfull', 'forecolor', 'pastetext', 'pasteword', 'removeformat', 'charmap', 'outdent', 'indent', 'undo', 'redo', 'wp_help' ), $editor_id),
	   			
	   			3 => apply_filters('mce_buttons_3', array(), $editor_id),
	   			
	   			4 => apply_filters('mce_buttons_4', array(), $editor_id),
	   			
	   		);

	   		
	   		// Basic
	   		$toolbars['Basic'] = array(
	   			
	   			1 => apply_filters( 'teeny_mce_buttons', array('bold', 'italic', 'underline', 'blockquote', 'strikethrough', 'bullist', 'numlist', 'justifyleft', 'justifycenter', 'justifyright', 'undo', 'redo', 'link', 'unlink', 'fullscreen'), $editor_id ),
	   			
	   		);
	   		
   		}
   		
   		
   		// Filter for 3rd party
   		$toolbars = apply_filters( 'acf/fields/wysiwyg/toolbars', $toolbars );
   		
   		
   		// return
	   	return $toolbars;
   	}
	
	/*
	*  render_field_settings()
	*
	*  Create extra settings for your field. These are visible when editing a field
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/
	
	function render_field_settings( $field ) {
	
		// vars
		$toolbars = $this->get_toolbars();
		$choices = array();
		
		if( !empty($toolbars) ) {
		
			foreach( $toolbars as $k => $v ) {
				
				$label = $k;
				$name = sanitize_title( $label );
				$name = str_replace('-', '_', $name);
				
				$choices[ $name ] = $label;
			}
		}
		
		/*
		*  acf_render_field_setting
		*
		*  This function will create a setting for your field. Simply pass the $field parameter and an array of field settings.
		*  The array of settings does not require a `value` or `prefix`; These settings are found from the $field array.
		*
		*  More than one setting can be added by copy/paste the above code.
		*  Please note that you must also have a matching $defaults value for the field name (font_size)
		*/
		
		acf_render_field_setting( $field, array(
			'label'			=> __('Save to','acf-FIELD_NAME'),
			'instructions'	=> __('How to save the field.','acf-FIELD_NAME'),
			'type'			=> 'select',
			'name'			=> 'post_field_type',
			//'prepend'		=> 'px',
			'choices' => array(
				'content'	=>	__("Content",'acf-FIELD_NAME'),
				'excerpt'	=>	__("Excerpt",'acf-FIELD_NAME'),
				'post_title'	=>	__("Title",'acf-FIELD_NAME')
			)
		));
		acf_render_field_setting( $field, array(
			'label'			=> __('Default Value','acf'),
			'instructions'	=> __('Appears when creating a new post.','acf'),
			'type'			=> 'text',
			'name'			=> 'default_value',
		));	
		acf_render_field_setting( $field, array(
			'label'			=> __('Toolbar','acf'),
			//'instructions'	=> __('Appears within the input.','acf'),
			'type'			=> 'radio',
			'name'			=> 'toolbar',
			'layout'	=>	'horizontal',
			'choices' => $choices
		));
		acf_render_field_setting( $field, array(
			'label'			=> __('Media Buttons','acf'),
			'instructions'	=> __('Show Media Upload Buttons?','acf'),
			'type'			=> 'radio',
			'name'			=> 'media_upload',
			'layout'	=>	'horizontal',
			'choices' => array(
				'yes'	=>	__("Yes",'acf'),
				'no'	=>	__("No",'acf'),
			)
		));
	}

	
	/*
	*  load_value()
	*
	*  This filter is appied to the $value after it is loaded from the db
	*
	*  @type	filter
	*  @since	1.0.0
	*  @date	23/01/13
	*
	*  @param	$value - the value found in the database
	*  @param	$post_id - the $post_id from which the value was loaded from
	*  @param	$field - the field array holding all the field options
	*
	*  @return	$value - the value to be saved in te database
	*/

	function load_value($value, $post_id, $field) {
		$value = content_field_load_value($value, $post_id, $field);
		return $value;
	}
}

new acf_post_field_wysiwyg();

?>