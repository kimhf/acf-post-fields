<?php
/*
Plugin Name: Advanced Custom Fields: Post Fields
Plugin URI: {{git_url}}
Description: {{short_description}}
Version: 1.0.0
Author: Kim Helge Frimanslund
Author URI: {{website}}
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/


class acf_field_content_field_plugin {
	/*
	*  Construct
	*
	*  @description:
	*  @since: 1.0.0
	*/

	function __construct() {
		// set text domain
		/*
		$domain = 'acf-content_field';
		$mofile = trailingslashit(dirname(__File__)) . 'lang/' . $domain . '-' . get_locale() . '.mo';
		load_textdomain( $domain, $mofile );
		*/
		
		// 2. Include field type for ACF5
		// $version = 5 and can be ignored until ACF6 exists
		// version 5+
		add_action('acf/include_field_types', array($this, 'include_field_types_textarea'));

		// version 4+
		add_action('acf/register_fields', array($this, 'register_fields'));
	}
	
	function include_field_types_textarea( $version ) {
		include_once('acf-post_image-v5.php');
		include_once('acf-post_textarea-v5.php');
		include_once('acf-post_text-v5.php');
		include_once('acf-post_wysiwyg-v5.php');
	}
	
	/*
	*  register_fields
	*
	*  @description:
	*  @since: 1.0.0
	*/

	function register_fields() {
		include_once('post_field_wysiwyg-v4.php');
		include_once('post_field_text-v4.php');
		include_once('post_field_textarea-v4.php');
		include_once('post_field_image-v4.php');
	}
}

new acf_field_content_field_plugin();

function content_field_load_value($value, $post_id, $field) {
	$post = get_post( $post_id, OBJECT, 'edit' );
	if ( $post->post_status != 'auto-draft' ) {
		if ( $field['post_field_type'] == 'post_title' ) {
			$value = $post->post_title;
		} else if ( $field['post_field_type'] == 'excerpt' ) {
			$value = $post->post_excerpt;
		} else if ( $field['post_field_type'] == 'content' ) {
			$value = $post->post_content;
		}
	}
	return $value;
}
?>