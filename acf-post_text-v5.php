<?php

class acf_post_field_text extends acf_field {
	
	/*
	*  __construct
	*
	*  Set name / label needed for actions / filters
	*
	*  @since	1.0.0
	*  @date	23/01/13
	*/
	
	function __construct() {
		// vars
		$this->name = 'post_text';
		$this->label = __("Post Text",'acf');
		$this->category = __('Post');
		$this->defaults = array(
			'default_value'	=>	'',
			'formatting' 	=>	'html',
			'maxlength'		=>	'',
			'placeholder'	=>	'',
			'prepend'		=>	'',
			'append'		=>	'',
			'post_field_type'	=>	'post_title',
		);
		
		// do not delete!
    	parent::__construct();
	}
	
	
	
	/*
	*  render_field()
	*
	*  Create the HTML interface for your field
	*
	*  @param	$field (array) the $field being rendered
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/
	
	function render_field( $field ) {
		$field['name'] = $field['post_field_type'];
	//var_dump($field);
		// vars
		$o = array( 'id', 'class', 'name', 'value', 'placeholder' );
		$e = '';
		
		
		// maxlength
		if( $field['maxlength'] !== "" )
		{
			$o[] = 'maxlength';
		}
		
		
		// prepend
		if( $field['prepend'] !== "" )
		{
			$field['class'] .= ' acf-is-prepended';
			$e .= '<div class="acf-input-prepend">' . $field['prepend'] . '</div>';
		}
		
		
		// append
		if( $field['append'] !== "" )
		{
			$field['class'] .= ' acf-is-appended';
			$e .= '<div class="acf-input-append">' . $field['append'] . '</div>';
		}
		
		
		$e .= '<div class="acf-input-wrap">';
		$e .= '<input type="text"';
		
		foreach( $o as $k )
		{
			$e .= ' ' . $k . '="' . esc_attr( $field[ $k ] ) . '"';	
		}
		
		$e .= ' />';
		$e .= '</div>';
		
		
		// return
		echo $e;
	}
	
	/*
	*  render_field_settings()
	*
	*  Create extra settings for your field. These are visible when editing a field
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/
	
	function render_field_settings( $field ) {
		
		/*
		*  acf_render_field_setting
		*
		*  This function will create a setting for your field. Simply pass the $field parameter and an array of field settings.
		*  The array of settings does not require a `value` or `prefix`; These settings are found from the $field array.
		*
		*  More than one setting can be added by copy/paste the above code.
		*  Please note that you must also have a matching $defaults value for the field name (font_size)
		*/
		
		acf_render_field_setting( $field, array(
			'label'			=> __('Save to','acf-FIELD_NAME'),
			'instructions'	=> __('How to save the field.','acf-FIELD_NAME'),
			'type'			=> 'select',
			'name'			=> 'post_field_type',
			//'prepend'		=> 'px',
			'choices' => array(
				'content'	=>	__("Content",'acf-FIELD_NAME'),
				'excerpt'	=>	__("Excerpt",'acf-FIELD_NAME'),
				'post_title'	=>	__("Title",'acf-FIELD_NAME')
			)
		));
		acf_render_field_setting( $field, array(
			'label'			=> __('Default Value','acf'),
			'instructions'	=> __('Appears when creating a new post.','acf'),
			'type'			=> 'text',
			'name'			=> 'default_value',
		));
		acf_render_field_setting( $field, array(
			'label'			=> __('Placeholder Text','acf'),
			'instructions'	=> __('Appears within the input.','acf'),
			'type'			=> 'text',
			'name'			=> 'placeholder',
		));
		acf_render_field_setting( $field, array(
			'label'			=> __('Prepend','acf'),
			'instructions'	=> __('Appears before the input.','acf'),
			'type'			=> 'text',
			'name'			=> 'prepend',
		));
		acf_render_field_setting( $field, array(
			'label'			=> __('Append','acf'),
			'instructions'	=> __('Appears after the input.','acf'),
			'type'			=> 'text',
			'name'			=> 'append',
		));
		acf_render_field_setting( $field, array(
			'label'			=> __('Character Limit','acf'),
			'instructions'	=> __('Leave blank for no limit.','acf'),
			'type'			=> 'text',
			'name'			=> 'maxlength',
		));
	}
	
	/*
	*  load_value()
	*
	*  This filter is appied to the $value after it is loaded from the db
	*
	*  @type	filter
	*  @since	1.0.0
	*  @date	23/01/13
	*
	*  @param	$value - the value found in the database
	*  @param	$post_id - the $post_id from which the value was loaded from
	*  @param	$field - the field array holding all the field options
	*
	*  @return	$value - the value to be saved in te database
	*/

	function load_value($value, $post_id, $field) {
		$value = content_field_load_value($value, $post_id, $field);
		return $value;
	}
}

new acf_post_field_text();

?>